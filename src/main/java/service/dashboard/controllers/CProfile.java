package service.dashboard.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import service.dashboard.models.Profile;
import service.dashboard.repositories.RProfile;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/api/profile")
public class CProfile {

    private RProfile rProfile;

    @Autowired
    public CProfile(RProfile rProfile) {
        this.rProfile = rProfile;
    }

    @GetMapping(value = "/all")
    public List<Profile> getAll() {
        return rProfile.findAll();
    }


    @PostMapping(value = "/post")
    public ResponseEntity<?> persist(@RequestBody final Profile profile) {
        HttpHeaders responseHeaders = new HttpHeaders();
        try {
            rProfile.save(profile);
            responseHeaders.add("Test", "Test");
            return new ResponseEntity<>(rProfile.findAll(), responseHeaders, HttpStatus.OK);
        } catch (Exception e) {
            responseHeaders.add("Error", e.toString());
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.BAD_REQUEST);
        }
    }
}
