package service.dashboard.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import service.dashboard.models.Application;

import service.dashboard.repositories.RApplication;

import java.util.Date;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/api/application")
public class CApplication {
    private RApplication rApplication;

    //    private BCryptPasswordEncoder bCryptPasswordEncoder;
    @Autowired
    public CApplication(RApplication rApplication) {
        this.rApplication = rApplication;
    }

    @GetMapping(value = "/all")
    public List<Application> getAll() {
        return rApplication.findByProfileShortCodeOrderByDateDesc("*144#");
    }

    @PostMapping(value = "/post")
    public ResponseEntity<?> persist(@RequestBody final Application application, @RequestHeader(value = "User-Agent") String userAgent) {
        HttpHeaders responseHeaders = new HttpHeaders();
        try {
            System.out.println("User-Agent => " + userAgent);
            responseHeaders.add("Test", "Test");
            rApplication.save(application);
            return new ResponseEntity<>(rApplication.findAll(), responseHeaders, HttpStatus.OK);
        } catch (Exception e) {
            responseHeaders.add("Error", e.toString());
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(value = "/update-heartbeat")
    public ResponseEntity<?> heartBit(@RequestBody final Application application) {
        try {
            if (rApplication.findByProfileShortCodeOrderByDateDesc(application.getProfile().getShortCode()).size() > 0) {
                Application applicationToBeEdited = rApplication.findByProfileShortCodeOrderByDateDesc(application.getProfile().getShortCode()).get(0);
                applicationToBeEdited.setHeartBit(new Date());
                rApplication.save(applicationToBeEdited);
                return new ResponseEntity<>(null, HttpStatus.OK);
            }
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }
}
