package service.dashboard.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import service.dashboard.models.Session;

public interface RSession  extends JpaRepository<Session, Long> {
}
