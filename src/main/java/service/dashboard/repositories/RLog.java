package service.dashboard.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import service.dashboard.models.Log;

public interface RLog  extends JpaRepository<Log, Long> {
}
