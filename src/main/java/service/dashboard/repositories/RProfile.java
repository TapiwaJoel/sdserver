package service.dashboard.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import service.dashboard.models.Profile;

public interface RProfile  extends JpaRepository<Profile, Long> {
}
