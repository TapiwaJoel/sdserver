package service.dashboard.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import service.dashboard.models.Application;
import java.util.List;

public interface RApplication extends JpaRepository<Application, Long> {
    List<Application> findByProfileShortCodeOrderByDateDesc(String shortCode);
}
