package service.dashboard;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@ComponentScan
@SpringBootApplication
@PropertySource({"classpath:application.properties"})
public class ServiceDashboardApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServiceDashboardApplication.class, args);
	}
}
