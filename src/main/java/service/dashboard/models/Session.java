package service.dashboard.models;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "sessions")
public class Session {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;


    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "profile_id")
    private Profile profile;

    @Column(name = "session_id")
    private String sessionId;

    @Column(name = "msisdn")
    private String msisdn;

    @Column(name = "short_code")
    private String shortCode;

    @Column(name = "status")
    private String status;

    @CreationTimestamp
    @Column(name = "start_time")
    private Date dateStarted;

    @CreationTimestamp
    @Column(name = "end_time")
    private Date dateEnded;

    @CreationTimestamp
    @Column(name = "date")
    private Date date;

    public Session() {
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getShortCode() {
        return shortCode;
    }

    public void setShortCode(String shortCode) {
        this.shortCode = shortCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getDateStarted() {
        return dateStarted;
    }

    public void setDateStarted(Date dateStarted) {
        this.dateStarted = dateStarted;
    }

    public Date getDateEnded() {
        return dateEnded;
    }

    public void setDateEnded(Date dateEnded) {
        this.dateEnded = dateEnded;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Session{" +
                "id=" + id +
                ", profile=" + profile +
                ", sessionId='" + sessionId + '\'' +
                ", msisdn='" + msisdn + '\'' +
                ", shortCode='" + shortCode + '\'' +
                ", status='" + status + '\'' +
                ", dateStarted=" + dateStarted +
                ", dateEnded=" + dateEnded +
                ", date=" + date +
                '}';
    }
}
