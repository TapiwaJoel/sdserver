package service.dashboard.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "profiles")
public class Profile {

    @Column(name = "name")
    private String name;

    @Id
    @Column(name = "short_code", unique = true)
    private String shortCode;

    public Profile() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortCode() {
        return shortCode;
    }

    public void setShortCode(String shortCode) {
        this.shortCode = shortCode;
    }

    @Override
    public String toString() {
        return "Profile{" +
                ", name='" + name + '\'' +
                ", shortCode='" + shortCode + '\'' +
                '}';
    }
}
