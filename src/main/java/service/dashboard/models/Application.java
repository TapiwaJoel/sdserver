package service.dashboard.models;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "applications")

public class Application {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "profile_short_code", referencedColumnName="short_code")
    private Profile profile;

    @CreationTimestamp
    @Column(name = "last_heart_bit")
    private Date heartBit;

    @CreationTimestamp
    @Column(name = "started_time")
    private Date dateStarted;

    @CreationTimestamp
    @Column(name = "date")
    private Date date;

    public Application() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public Date getHeartBit() {
        return heartBit;
    }

    public void setHeartBit(Date heartBit) {
        this.heartBit = heartBit;
    }

    public Date getDateStarted() {
        return dateStarted;
    }

    public void setDateStarted(Date dateStarted) {
        this.dateStarted = dateStarted;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }


    @Override
    public String toString() {
        return "Application{" +
                "id=" + id +
                ", profile=" + profile +
                ", heartBit=" + heartBit +
                ", dateStarted=" + dateStarted +
                ", date=" + date +
                '}';
    }
}
